package com.github.pabloo99;

import com.github.pabloo99.dao.EmployeeDao;
import static org.testng.Assert.assertTrue;

import com.github.pabloo99.entity.Employee;
import org.testng.annotations.Test;

public class EmployeeDaoTest {

    @Test
   public void checkIfReturnAllEmployees() {
        EmployeeDao employeeDao = new EmployeeDao();

        assertTrue(employeeDao.findAll().size() == 107);
    }

    @Test
    public void checkIfReturnPersonWithExactId() {
        EmployeeDao employeeDao = new EmployeeDao();

        assertTrue(employeeDao.findEmployeeById(100).getLastName().equals("King"));
    }

}

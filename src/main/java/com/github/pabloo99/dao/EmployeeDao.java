package com.github.pabloo99.dao;

import com.github.pabloo99.connection.HibernateUtil;
import com.github.pabloo99.entity.Employee;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



public class EmployeeDao {

    public List<Employee> findAll() {
       Session session = HibernateUtil.getSessionFactory().openSession();
       Transaction ts = null;

       try {
            ts = session.beginTransaction();
            List<Employee> employeesList = new ArrayList<>();

            Iterator iterator = session.createQuery("FROM Employee").list().iterator();
            while(iterator.hasNext()) {
                Employee employee = (Employee) iterator.next();
                employeesList.add(employee);
            }
            return employeesList;
       } catch (HibernateException e) {
           if (ts!=null) ts.rollback();
           e.printStackTrace();
       } finally {
           session.close();
       }
       return null;
    }

    public void saveEmployee(Employee employee) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = null;

        try {
            ts = session.beginTransaction();

            session.save(employee);

            ts.commit();

        } catch (HibernateException e) {
            if (ts!=null) ts.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Employee findEmployeeById(Integer employeeID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction ts = null;

        try {
            ts = session.beginTransaction();
            Employee employee;

            Query query = session.createQuery("FROM Employee E WHERE E.id = :employee_id");

            query.setParameter("employee_id", employeeID);

            employee = (Employee) query.getSingleResult();

            return employee;

        } catch (HibernateException e) {
            if (ts!=null) ts.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }




}

package com.github.pabloo99.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.sql.Date;
@Data
@NoArgsConstructor

public class Employee {

    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Date hireDate;
    private String jobID;
    private Double salary;
    private Double commissionPCT;
    private Integer managerID;
    private Integer departmentID;

    public Employee(Integer id, String firstName, String lastName, String email, String jobID) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.hireDate = Date.valueOf(LocalDate.now());
        this.jobID = jobID;
    }

}
